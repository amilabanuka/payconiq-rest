package com.banuka.payconiq.test.exception;

/**
 * Thrown when the repository operation is failed due to an error condition
 */
public class DataAccessException extends Exception {

  public DataAccessException(String s) {
    super(s);
  }
}
