package com.banuka.payconiq.test.model.service;

import java.time.Instant;

/**
 * Service data model where the data is stored in the data store as well
 */
public class StockModel {

  private int id;
  private String name;
  private double currentPrice;
  private Instant lastUpdated;
  private int version;

  public StockModel(int id, String name, double currentPrice) {
    this.id = id;
    this.name = name;
    this.currentPrice = currentPrice;
    lastUpdated = Instant.now();
    version = 0;
  }

  public StockModel(String name, double currentPrice) {
    id = -1;
    this.name = name;
    this.currentPrice = currentPrice;
    lastUpdated = Instant.now();
    version = 0;
  }

  public void increaseVersion() {
    this.version++;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public double getCurrentPrice() {
    return currentPrice;
  }

  public Instant getLastUpdated() {
    return lastUpdated;
  }

  public int getVersion() {
    return version;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCurrentPrice(double currentPrice) {
    this.currentPrice = currentPrice;
  }

  public void setLastUpdated(Instant lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    StockModel that = (StockModel) o;
    return !(this.id == -1 || that.getId() == -1)
        && new org.apache.commons.lang3.builder.EqualsBuilder().append(id, that.id).isEquals();
  }

  @Override
  public int hashCode() {
    return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
        .append(id)
        .toHashCode();
  }
}
