package com.banuka.payconiq.test.service;

import com.banuka.payconiq.test.exception.DataAccessException;
import com.banuka.payconiq.test.model.api.Stock;
import com.banuka.payconiq.test.model.service.StockModel;
import com.banuka.payconiq.test.repository.StockRepository;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to support the API services
 */
@Service
public class StockManagerService {

  private static final Logger logger = LoggerFactory.getLogger(StockManagerService.class);

  @Autowired
  private StockRepository stockRepository;

  /**
   * Returns all the stocks in the system in API data format
   *
   * @return list of Stock objects
   */
  public List<Stock> getAllStocks() {
    logger.info("Returning all stocks in the store");
    return stockRepository.findAll().stream().map(this::mapDomainToApi)
        .collect(Collectors.toList());
  }

  /**
   * find a single stock by id
   *
   * @param id : id to search
   * @return stock if found, null otherwise
   */
  public Stock findById(int id) {
    logger.info("Searching for the stock with id [" + id + "]");
    StockModel stockModel = stockRepository.find(id);
    logger.debug("Mapping the data model to API");
    return mapDomainToApi(stockModel);
  }

  /**
   * update an existing stock
   *
   * @param stock : stock to update
   * @return status of the update operation
   */
  public boolean updatePrice(Stock stock) {
    StockModel stockModel = stockRepository.find(stock.getId());
    if (stockModel == null) {
      return false;
    }
    logger.debug("Entry found. injecting the data");
    injectToDomain(stock, stockModel);
    try {
      logger.debug("Saving the data to store");
      stockRepository.saveOrUpdate(stockModel);
      logger.info("Update complete");
      return true;
    } catch (DataAccessException e) {
      logger.error("update failed. returning not found");
      return false;
    }
  }


  /**
   * crete a new stock
   *
   * @param stock stock to create
   * @return created stock for display
   */
  public Stock create(Stock stock) {
    logger.info("Creating a new stock with name ["+stock.getName()+"]");
    StockModel stockModel = new StockModel(stock.getName(), stock.getCurrentPrice());
    try {
      return mapDomainToApi(stockRepository.saveOrUpdate(stockModel));
    } catch (DataAccessException e) {
      logger.error("Saving failed [" + e.getMessage() + "]");
      return null;
    }
  }

  private void injectToDomain(Stock stock, StockModel stockModel) {
    stockModel.setCurrentPrice(stock.getCurrentPrice());
  }


  private Stock mapDomainToApi(StockModel stockModel) {
    if (stockModel == null) {
      return null;
    }
    Stock stock = new Stock();
    stock.setId(stockModel.getId());
    stock.setName(stockModel.getName());
    stock.setCurrentPrice(stockModel.getCurrentPrice());
    stock.setLastUpdated(Date.from(stockModel.getLastUpdated()));
    logger.debug("Mapping the data model to API completed");
    return stock;
  }


}
