package com.banuka.payconiq.test.config;

import com.banuka.payconiq.test.api.StocksApi;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.banuka.payconiq.test"})
public class AppConfig {

  @Autowired private StocksApi stocksApi;

  @Bean(destroyMethod = "shutdown")
  public SpringBus cxf() {
    return new SpringBus();
  }

  @Bean(destroyMethod = "destroy")
  @DependsOn("cxf")
  public Server jaxRsServer() {
    final JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();

    factory.setServiceBean(stocksApi);
    factory.setProvider(new JacksonJsonProvider());
    factory.setBus(cxf());
    factory.setAddress("/");

    return factory.create();
  }

  @Bean
  public ServletRegistrationBean cxfServlet() {
    final ServletRegistrationBean servletRegistrationBean =
        new ServletRegistrationBean(new CXFServlet(), "/api/*");
    servletRegistrationBean.setLoadOnStartup(1);
    return servletRegistrationBean;
  }
}
