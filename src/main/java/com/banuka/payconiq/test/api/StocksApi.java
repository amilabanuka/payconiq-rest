package com.banuka.payconiq.test.api;

import com.banuka.payconiq.test.model.api.Stock;
import com.banuka.payconiq.test.service.StockManagerService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Rest api endpoint to expose the API
 */
@Service
@Path("/stocks")
public class StocksApi {

  private static final Logger logger = LoggerFactory.getLogger(StocksApi.class);

  @Autowired
  private StockManagerService stockManagerService;

  /**
   * return all the stocks in the system
   *
   * @return all the stocks
   */
  @GET
  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Stock> getAllStocks() {
    logger.info("Calling get all stocks");
    return stockManagerService.getAllStocks();
  }

  /**
   * returns the stock identified by the id provided
   *
   * @param id : id of the stock
   * @return stock with the id @id
   */
  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Stock getSingleStocks(@PathParam("id") int id) {
    logger.info("Calling get all stocks");
    return stockManagerService.findById(id);
  }

  /**
   * update a stock identified by the id provided with the stock object in the message body
   *
   * @param stock json representation of the stock
   */
  @PUT
  @Path("/{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response updateSingleStock(Stock stock, @PathParam("id") int id) {
    logger.info("Updating the stock");
    stock.setId(id);
    boolean result = stockManagerService.updatePrice(stock);
    if (!result) {
      logger.error("The stock not found in the data store");
      return Response.status(Status.NOT_FOUND).build();
    }
    logger.info("Stock successfully updated");
    return Response.accepted().build();
  }

  /**
   * create a stock with the parameters provided in the message body
   *
   * @param stock json representation of the stock
   * @return created stock object
   */
  @POST
  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public Stock createStock(Stock stock) {

    return stockManagerService.create(stock);
  }

}
