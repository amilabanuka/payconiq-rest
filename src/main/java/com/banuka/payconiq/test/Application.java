package com.banuka.payconiq.test;


import com.banuka.payconiq.test.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    new SpringApplication(AppConfig.class).run(args);
//    SpringApplication.run(AppConfig.class, args);
  }
}
