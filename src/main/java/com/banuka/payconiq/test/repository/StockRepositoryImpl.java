package com.banuka.payconiq.test.repository;

import com.banuka.payconiq.test.exception.DataAccessException;
import com.banuka.payconiq.test.model.service.StockModel;
import java.time.Instant;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * In memory implementation of the stock API
 */
@Component
public class StockRepositoryImpl implements StockRepository {

  private Map<Integer, StockModel> dataStore;
  private AtomicInteger currentHigh;

  public StockRepositoryImpl() {
    dataStore = new ConcurrentHashMap<>();
    currentHigh = new AtomicInteger(0);
  }

  @Override
  public StockModel find(Integer id) {
    if (id == null) {
      return null;
    }
    StockModel stockModel = dataStore.get(id);
    if (stockModel == null) {
      return null;
    }

    return deepClone(stockModel);
  }

  @Override
  public StockModel saveOrUpdate(StockModel stockModel) throws DataAccessException {
    StockModel existing = dataStore.get(stockModel.getId());
    if (existing == null) {
      StockModel target = new StockModel(currentHigh.incrementAndGet(), stockModel.getName(),
          stockModel.getCurrentPrice());
      dataStore.put(target.getId(), target);
      return deepClone(target);
    } else {
      synchronized (existing) {
        if (existing.getVersion() <= stockModel.getVersion()) {
          existing.setName(stockModel.getName());
          existing.setCurrentPrice(stockModel.getCurrentPrice());
          existing.setLastUpdated(Instant.now());
          existing.increaseVersion();
          return deepClone(existing);
        } else {
          throw new DataAccessException("Dirty write. Data is updated");
        }
      }
    }
  }

  @Override
  public Collection<StockModel> findAll() {
    return dataStore.values().stream().map(this::deepClone).collect(Collectors.toList());
  }


  private StockModel deepClone(StockModel source) {
    StockModel stockModel = new StockModel(source.getId(), source.getName(),
        source.getCurrentPrice());
    stockModel.setLastUpdated(source.getLastUpdated());
    stockModel.setVersion(source.getVersion());
    stockModel.setId(stockModel.getId());
    return stockModel;
  }
}
