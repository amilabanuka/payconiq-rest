package com.banuka.payconiq.test.repository;

import com.banuka.payconiq.test.exception.DataAccessException;
import java.util.Collection;

/**
 * Abstract representation of a data access repository.
 *
 * @param <T> : type of the object the repository is service
 * @param <V> : key type of the repository object used to identify the object uniquely
 */
public interface Repository<T, V> {

  /**
   * find the object from the repository data source using the key of type V provided
   *
   * @param v : value of the primary identification key
   * @return the object if found and returns null if not found in the repository backing data source
   */
  T find(V v);

  /**
   * save or update exising object in the repository backing data source.
   *
   * @param t : object to be created or updated
   * @return T : returns the created object back
   * @throws DataAccessException if the save or update operation fails.
   */
  T saveOrUpdate(T t) throws DataAccessException;

  /**
   * Returns all the data in the data store
   *
   * @return collection of data of the data store
   */
  Collection<T> findAll();


}
