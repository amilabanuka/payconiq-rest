package com.banuka.payconiq.test.repository;

import com.banuka.payconiq.test.model.service.StockModel;

/**
 * Repository specialization for Stock data model
 */
public interface StockRepository extends Repository<StockModel, Integer> {

}
