package com.banuka.payconiq.test.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.fail;

import com.banuka.payconiq.test.exception.DataAccessException;
import com.banuka.payconiq.test.model.api.Stock;
import com.banuka.payconiq.test.model.service.StockModel;
import com.banuka.payconiq.test.repository.StockRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by amila on 3/15/2018.
 */
public class StockManagerServiceTest {

  private StockManagerService service;

  @Mock
  private StockRepository stockRepository;

  @BeforeMethod
  public void init() {
    service = new StockManagerService();
    stockRepository = Mockito.mock(StockRepository.class);
    ReflectionTestUtils.setField(service, "stockRepository", stockRepository);
  }

  @Test
  public void shouldReturnAll() {
    Set<StockModel> stockModels = new HashSet<>(2);
    StockModel stockModel1 = new StockModel(1, "a1", 1.5);
    StockModel stockModel2 = new StockModel(2, "a2", 2.5);
    stockModels.add(stockModel1);
    stockModels.add(stockModel2);
    when(stockRepository.findAll()).thenReturn(stockModels);
    List<Stock> stocks = service.getAllStocks();
    assertEquals(2, stocks.size());

  }


  @Test
  public void shouldReturnNullIfRepoReturnNull() {
    when(stockRepository.find(1)).thenReturn(null);
    Stock stock = service.findById(1);
    assertNull(stock);
  }

  @Test
  public void shouldReturnConvertedIfRepoReturns() {
    StockModel stockModel = new StockModel(1, "a1", 1.5);
    when(stockRepository.find(1)).thenReturn(stockModel);
    Stock stock = service.findById(1);
    assertNotNull(stock);
    assertEquals(1, stock.getId());

  }

  @Test
  public void shouldReturnFalseIfNotFound() {
    Stock stock = new Stock();
    stock.setId(1);
    when(stockRepository.find(1)).thenReturn(null);
    assertFalse(service.updatePrice(stock));
  }

  @Test
  public void shouldReturnFalseIfError() {
    Stock stock = new Stock();
    stock.setId(1);
    StockModel stockModel = new StockModel(1, "a1", 1.5);
    when(stockRepository.find(1)).thenReturn(stockModel);
    try {
      when(stockRepository.saveOrUpdate(stockModel)).thenThrow(new DataAccessException("test"));
    } catch (DataAccessException e) {
      fail();
    }
    assertFalse(service.updatePrice(stock));
  }

  @Test
  public void shouldReturnTrueIfSuccess() {
    Stock stock = new Stock();
    stock.setId(1);
    StockModel stockModel = new StockModel(1, "a1", 1.5);
    when(stockRepository.find(1)).thenReturn(stockModel);
    try {
      when(stockRepository.saveOrUpdate(any(StockModel.class))).thenReturn(stockModel);
    } catch (DataAccessException e) {
      e.printStackTrace();
      fail("No Exception Expected");
    }
    assertTrue(service.updatePrice(stock));
  }


  @Test
  public void shouldReturnDataIfCreated() {
    Stock stock = new Stock();
    stock.setName("test");
    stock.setCurrentPrice(1.5);
    StockModel stockModel = new StockModel(1, "test", 1.5);
    try {
      when(stockRepository.saveOrUpdate(any(StockModel.class))).thenReturn(stockModel);
    } catch (DataAccessException e) {
      fail();
    }
    Stock result = service.create(stock);
    assertEquals("test", result.getName());
    assertEquals(1.5, result.getCurrentPrice());

  }

  @Test
  public void shouldReturnNullIfUpdateFailed() {
    Stock stock = new Stock();
    stock.setName("test");
    stock.setCurrentPrice(1.5);
    try {
      when(stockRepository.saveOrUpdate(any(StockModel.class)))
          .thenThrow(new DataAccessException("test"));
    } catch (DataAccessException e) {
      fail();
    }
    Stock result = service.create(stock);
    assertNull(result);


  }

}
