package com.banuka.payconiq.test.repository;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.fail;

import com.banuka.payconiq.test.exception.DataAccessException;
import com.banuka.payconiq.test.model.service.StockModel;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by amila on 3/15/2018.
 */
@Test
public class StockRepositoryImplTest {

  private StockRepositoryImpl service;


  @BeforeMethod
  public void init() {
    service = new StockRepositoryImpl();
  }


  @Test
  public void shouldReturnNullIfTheObjectIsNotFound() {
    StockModel result = service.find(1);
    assertNull(result);
  }

  @Test
  public void shouldReturnNullIfPassedNull() {
    StockModel result = service.find(null);
    assertNull(result);
  }

  @Test
  public void shouldReturnIfTheObjectIsAvailable() {
    StockModel stockModel = new StockModel(1, "test", 2.3);

    Map<Integer, StockModel> stockModelMap = new ConcurrentHashMap<>();
    stockModelMap.put(1, stockModel);
    ReflectionTestUtils.setField(service, "dataStore", stockModelMap);

    StockModel result = service.find(1);
    assertEquals(result.getName(), "test");
    assertEquals(result.getCurrentPrice(), 2.3);
    assertEquals(result.getId(), 1);

  }

  @Test
  public void shouldReturnAClone() {
    StockModel stockModel = new StockModel(1, "test", 2.3);

    Map<Integer, StockModel> stockModelMap = new ConcurrentHashMap<>();
    stockModelMap.put(1, stockModel);
    ReflectionTestUtils.setField(service, "dataStore", stockModelMap);

    StockModel result = service.find(1);
    assertTrue(stockModel != result);
    assertTrue(stockModel.equals(result));

  }


  @Test
  public void shouldCreteNewIfNotAvailable() {
    StockModel stockModel = new StockModel("test", 2.3);
    try {
      StockModel result = service.saveOrUpdate(stockModel);
      assertEquals(1, result.getId());
    } catch (DataAccessException e) {
      fail("No Exception expected");
    }
  }

  @Test
  public void shouldUpdate() {
    StockModel stockModel = new StockModel("test", 2.3);
    try {
      StockModel result = service.saveOrUpdate(stockModel);
      result.setCurrentPrice(5);
      result = service.saveOrUpdate(result);
      assertEquals(1, result.getId());
      assertEquals(5d, result.getCurrentPrice());
      assertEquals(1, result.getVersion());
    } catch (DataAccessException e) {
      fail("No Exception expected");
    }
  }

  @Test
  public void shouldFailIfTheRepositoryUpdated() {
    StockModel stockModel = new StockModel("test", 2.3);
    try {
      StockModel result = service.saveOrUpdate(stockModel);
      result.setCurrentPrice(5);

      StockModel result1 = service.saveOrUpdate(result);
      assertEquals(5d, result1.getCurrentPrice());
      assertEquals(1, result1.getVersion());
      result.setCurrentPrice(6);
      service.saveOrUpdate(result);
      fail("Exception expected");
    } catch (DataAccessException e) {

    }
  }


  @Test
  public void shouldReturnAll() {
    StockModel stockModel = new StockModel("test", 2.3);
    StockModel stockMode2 = new StockModel("test", 2.3);
    try {
      service.saveOrUpdate(stockModel);
      service.saveOrUpdate(stockMode2);
    } catch (DataAccessException e) {
      fail("No Exception expected");
    }
    Collection<StockModel> all = service.findAll();
    assertEquals(2, all.size());
  }


}
