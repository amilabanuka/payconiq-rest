package com.banuka.payconiq.test.model.service;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

/**
 * Created by amila on 3/15/2018.
 */
@Test
public class StockModelTest {


  @Test
  public void shouldCreateTheObjectWithValues() {
    StockModel stockModel = new StockModel(1, "test", 1.5);
    assertEquals(stockModel.getId(), 1);
    assertEquals(stockModel.getName(), "test");
    assertEquals(stockModel.getCurrentPrice(), 1.5);
    assertEquals(stockModel.getVersion(), 0);
  }



}
