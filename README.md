# README #

Stock management API with simple create, update, get all and get selected operations

### Operations Available ###

* Create Stock
* update Stock
* read all stock
* read single stock



### Source Code and build requirement ###

* Repository URL : https://bitbucket.org/amilabanuka/payconiq-rest
* JDK8
* Maven 3

### build and execution ###

 checking out

  clone or download the project from 

         https://bitbucket.org/amilabanuka/payconiq-rest

- Build

         mvn package
         
- Run using Spring Boot Maven plugin

         mvn spring-boot:run
         
- Run using standalone JAR

         java -jar target/stock-api-1.0-SNAPSHOT.jar

- Invoke  web services

        curl http://localhost:8080/api/stocks
        
        curl http://localhost:8080/api/stocks/1
        
        curl -H "Content-Type: application/json" -X POST -d '{"name": "A2","currentPrice":5.5}' http://localhost:8080/api/stocks
        
        curl -H "Content-Type: application/json" -X PUT -d '{"currentPrice":6.5}' http://localhost:8080/api/stocks/1

- create new stock

        curl -H "Content-Type: application/json" -X POST 
          -d '{"name": "A2","currentPrice":5.5}' http://localhost:8080/api/stocks

This creates a new Stock and return the stock details

```json
{
    "id": 2,
    "name": "A2",
    "currentPrice": 5.5,
    "lastUpdated": "16-03-2018 01:54:53"
}
```

- update stock

        curl -H "Content-Type: application/json" -X PUT 
          -d '{"currentPrice":6.5}' http://localhost:3000/api/stocks/1

This updates the stock with new price and returns success status code if the operation completes successfully


- Get all Stocks

        curl http://localhost:8080/api/stocks

Returns all the stocks in the system

```json
[
    {
        "id": 1,
        "name": "A1",
        "currentPrice": 3.5,
        "lastUpdated": "16-03-2018 01:54:47"
    },
    {
        "id": 2,
        "name": "A2",
        "currentPrice": 7.5,
        "lastUpdated": "16-03-2018 02:14:45"
    }
]
```


- Get Single Stock

        curl http://localhost:8080/api/stocks/1

Returns all the stocks in the system

```json
{
    "id": 1,
    "name": "A1",
    "currentPrice": 3.5,
    "lastUpdated": "16-03-2018 01:54:47"
}
```

